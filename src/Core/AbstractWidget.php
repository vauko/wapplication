<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace StudentMarketing\Core;


use StudentMarketing\Model\TicketProvider;

abstract class AbstractWidget extends \WP_Widget {
	protected $provider;
	protected $settings;
	protected $template;

	/**
	 * AbstractWidget constructor.
	 *
	 * @param string $id_base
	 * @param string $name
	 * @param array $widget_options
	 * @param array $control_options
	 *
	 * @throws \Exception
	 */
	public function __construct( $id_base, $name, array $widget_options = [], array $control_options = [] ) {
		parent::__construct( $id_base, $name, $widget_options, $control_options );

		$this->settings = Settings::getInstance();
		$this->template = Template::getInstance();
		$this->provider = TicketProvider::getInstance();
	}
}