<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace PluginApplication\Core;


/**
 * Class AbstractCommon
 * @package StudentMarketing\Common
 */
abstract class AbstractCommon {

	protected $registry;

	public function __construct( $registry ) {
		$this->registry = $registry;
	}

	public function __get( $key ) {
		return $this->registry->get( $key );
	}

	public function __isset( $key ) {
		return $this->registry->has( $key ) !== null;
	}

	public function __set( $key, $value ) {
		$this->registry->set( $key, $value );
	}
}