<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace PluginApplication\Core;


final class Registry {
	private $registry;

	public function __construct() {
		$this->registry = new \stdClass();

	}

	public function get( $key ) {
		return ( $this->registry->$key ?? null );
	}

	public function set( $key, $value ) {
		$this->registry->$key = $value;
	}

	public function has( $key ): bool {
		return isset( $this->registry->$key );
	}
}