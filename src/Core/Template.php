<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace PluginApplication\Core;

/**
 * Class Template
 * @package PluginApplication\Core
 */
class Template extends AbstractCommon {
	private $template;
	private $data = [];
	private $callback;

	/**
	 * Template constructor.
	 *
	 * @param $registry
	 * @param $callback
	 */
	public function __construct( $registry, $callback ) {
		parent::__construct( $registry );

		$this->callback = $callback;
	}

	public function __call( $name, $arguments ) {
		if ( $this->callback !== null ) {
			return \call_user_func_array([$this->callback, $name], $arguments );
		}
	}

	/**
	 * Add keys and their values.
	 *
	 * @param string $key get key
	 * @param string $value get key value
	 *
	 * @return Template current object
	 */
	public function set( $key, $value ): Template {
		$this->data[ $key ] = $value;

		return $this;
	}

	/**
	 * Render the template file
	 *
	 * @param string $template Template foo/bar.
	 *
	 * @return string
	 */
	public function render( $template ): string {
		$this->template = $template;
		$file           = $this->getViewFile();

		if ( $file === false ) {
			return "Error: Could not load template ($this->template)!";

		}

		$countVariablesCreated = extract( $this->data, EXTR_SKIP );
		if ( $countVariablesCreated !== \count( $this->data ) ) {
			throw new \RuntimeException( 'Extraction failed: scope modification attempted' );
		}

		ob_start();
		include $file;

		return ob_get_clean();
	}

	/**
	 * Looks for the view file according to the given view name.
	 *
	 * @param string $extension
	 *
	 * @return mixed the view file path, false if the view file does not exist
	 */
	public function getViewFile( $extension = 'php' ) {
		$viewPath = $this->registry->get( 'config' )->path . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'View';

		$path[] = realpath( $viewPath . DIRECTORY_SEPARATOR . $this->template . '_custom.' . $extension );
		$path[] = realpath( $viewPath . DIRECTORY_SEPARATOR . $this->template . '.' . $extension );

		foreach ( $path as $file ) {
			if ( $file !== false ) {
				return $file;
			}
		}

		return false;
	}
}