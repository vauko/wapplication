<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace StudentMarketing\Core;


class Collection {
	private $limit = 100;
	private $offset = 1;

	/**
	 * Total Entries
	 *
	 * @var int
	 */
	private $total = 0;

	private $collection = [];

	private $collectionName;

	public function __construct( $name = '' ) {
		$this->collectionName = $name;
	}

	/**
	 * @return int
	 */
	public function getLimit(): int {
		return $this->limit;
	}

	/**
	 * @return int
	 */
	public function getOffset(): int {
		return $this->offset;
	}

	/**
	 * Return total entries.
	 *
	 * @return int|null
	 */
	public function getTotal(): int {
		if ( $this->total == 0 ) {
			return $this->count();
		}

		return $this->total;
	}

	/**
	 * Return collection total entries
	 *
	 * @return int
	 */
	public function count(): int {
		return count( $this->collection );
	}

	/**
	 * Check if collection is empty.
	 *
	 * @return bool
	 */
	public function isEmpty(): bool {
		return count( $this->collection ) == 0;
	}

	/**
	 * @return array
	 */
	public function getCollection(): array {
		return $this->collection;
	}

	public function getCollectionName(): string {
		return $this->collectionName;
	}

	/**
	 * @param int $limit
	 */
	public function setLimit( int $limit ): void {
		$this->limit = $limit;
	}

	/**
	 * @param int $offset
	 */
	public function setOffset( int $offset ): void {
		$this->offset = $offset;
	}

	/**
	 * Total entries setter.
	 *
	 * @param int $total
	 */
	public function setTotal( int $total ): void {
		$this->total = $total;
	}

	/**
	 * @param array $collection
	 */
	public function setCollection( array $collection ): void {
		$this->collection = $collection;
	}

	/**
	 * Convert collection items from array to object
	 *
	 * @param array $collection Collection
	 * @param string $class Custom class specified for item.
	 */
	public function setCollectionAsObjects( array $collection, $class = 'stdClass' ): void {
		$this->collection = [];

		foreach ( $collection as $item ) {
			$this->collection[] = new $class( $item );
		}
	}
}