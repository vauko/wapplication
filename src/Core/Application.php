<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace PluginApplication\Core;

use Exception;
use PluginApplication\Exceptions\ApplicationException;

/**
 * Class Application
 * @package Application\Common
 */
class Application {

	/**
	 * @var Option Application registry
	 */
	private $registry;

	private $exception;

	/**
	 * Application constructor.
	 *
	 * @param string $path Full dir path to the plugin application
	 * @param string $nameSpace
	 *
	 * @throws \Exception
	 */
	public function __construct( $path, $nameSpace ) {

		try {
			$this->registry = new Registry();
			$this->loadConfig( $path, $nameSpace );

			$this->autoloader();

			$option = new Option( $this->registry );
			$this->registry->set( 'option', $option );

			$loader = new Loader( $this->registry );
			$this->registry->set( 'load', $loader );

			$this->registerPostTypes();
			$this->addHooks();
			$this->loadShortcodes();

			if ( is_admin() ) {
				// Admin Controller
				$loader->loadMaintenance();

				$this->load->controller( 'Backend' );
				$this->registry->get( 'BackendController' )->run();
			} else {
				// Front Controller
				$this->load->controller( 'Front' );
				$this->registry->get( 'FrontController' )->run();
			}

			//throw new ApplicationException( 'TEST' );
		} catch ( ApplicationException $exception ) {
			$this->exception = $exception;

			echo $exception->getMessage();

			if ( $this->config->deactivateOnError ) {
				add_action( 'admin_notices', [ $this, 'showNotice' ] );
				add_action( 'admin_init', [ $this, 'deactivatePlugin' ] );
			}
		}
	}

	public function __get( $key ) {
		return $this->registry->get( $key );
	}

	public function __isset( $key ) {
		return $this->registry->has( $key );
	}

	public function __set( $key, $value ) {
		$this->registry->set( $key, $value );
	}

	/**
	 *
	 */
	public function deactivatePlugin() {
		$plugin = end( explode( DIRECTORY_SEPARATOR, $this->config->path ) );
		$plugin = $plugin . DIRECTORY_SEPARATOR . $plugin . '.php';
		\deactivate_plugins( $plugin );
	}

	public function showNotice() {
		$class   = 'notice notice-error';
		$message = __( 'Plugin Application Error: ' . $this->exception->getMessage(), 'text-domain' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );

		$deactivated = __( 'Plugin ' . $this->option->get( 'applicationName' ) . ' deactivated! Use Config file to change behavior.', 'text-domain' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $deactivated ) );
	}

	// Private Methods
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Load common config file
	 *
	 * @param string $path
	 * @param string $nameSpace
	 */
	private function loadConfig( $path, $nameSpace ) {
		$applicationPath = plugin_dir_path( $path );

		$configPath = $applicationPath . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'Config.php';

		if ( ! file_exists( $configPath ) ) {
			throw new ApplicationException( 'Config file not found.' );
		}

		include $configPath;

		try {
			$className = $nameSpace . '\\Config\\Config';
			$config    = new $className();

			$config->path      = $applicationPath;
			$config->nameSpace = $nameSpace;
			$config->url       = plugin_dir_url( $path );

			$this->config = $config;
		} catch ( \RuntimeException $exception ) {
			throw new ApplicationException( 'Can\'t load the config.' );
		}
	}

	public function loadShortcodes() {
		$path = $this->config->path . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'Shortcode';

		if ( ! is_dir( $path ) ) {
			return;
		}

		$shortcodes = scandir( $path, SCANDIR_SORT_NONE );

		foreach ( $shortcodes as $shortcode ) {
			if ( ! \in_array( $shortcode, [
					'.',
					'..'
				] ) && is_file( $path . DIRECTORY_SEPARATOR . $shortcode ) ) {
				$shortcode = basename( $shortcode, '.php' );
				$class     = $this->config->nameSpace . '\\Shortcode\\' . $shortcode;
				$object    = new $class( $this->registry );

				if ( isset( $object->name ) && \is_string( $object->name ) ) {
					$shortcode = $object->name;
				}

				add_shortcode( $shortcode, [ $object, 'init' ] );
			}
		}
	}

	/**
	 * Autoload application classes
	 *
	 * @throws \Exception
	 */
	private function autoloader() {
		try {
			spl_autoload_register( function ( $class ) {
				// project-specific namespace prefix
				$prefix = $this->config->nameSpace . '\\';

				// base directory for the namespace prefix
				$dir = $this->config->path . '/application/';

				// does the class use the namespace prefix?
				$len = \strlen( $prefix );
				if ( strncmp( $prefix, $class, $len ) !== 0 ) {
					// no, move to the next registered autoloader
					return;
				}

				// get the relative class name
				$relative_class = substr( $class, $len );

				// replace the namespace prefix with the base directory, replace namespace
				// separators with directory separators in the relative class name, append
				// with .php
				$file = $dir . str_replace( '\\', '/', $relative_class ) . '.php';

				// if the file exists, require it
				if ( file_exists( $file ) ) {
					require $file;
				}
			} );
		} catch ( Exception $e ) {
			throw new ApplicationException( $e->getMessage() );
		}
	}

	private function registerPostTypes() {
		if ( isset( $this->config->postTypes ) && \is_array( $this->config->postTypes ) ) {
			foreach ( $this->config->postTypes as $postType ) {
				add_action( 'init', [ $this->config, 'registerPostType_' . $postType['name'] ] );
			}
		}
	}

	private function addHooks() {
		$types = [
			0 => [
				'field'    => 'actions',
				'function' => 'add_action'
			],
			1 => [
				'field'    => 'filters',
				'function' => 'add_filter'
			]
		];

		foreach ( $types as $type ) {
			$field    = $type['field'];
			$function = $type['function'];

			if ( isset( $this->config->$field ) && \is_array( $this->config->$field ) ) {
				foreach ( $this->config->$field as $action ) {
					$controllerName = $action['callback']['controller'] . 'Controller';

					if ( ! isset( $this->$controllerName ) ) {
						$this->load->controller( $action['callback']['controller'] );
					}

					$callback = [
						$this->$controllerName,
						$action['callback']['action']
					];

					if ( isset( $action['priority'] ) ) {
						if ( isset( $action['arguments'] ) ) {
							$function( $action['name'], $callback, $action['priority'], $action['arguments'] );
						} else {
							$function( $action['name'], $callback, $action['priority'] );
						}
					} else {
						$function( $action['name'], $callback );
					}
				}
			}
		}
	}
}