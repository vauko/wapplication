<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace PluginApplication\Core;

/**
 * Class Loader
 * @package PluginApplication\Core
 */
class Loader {
	protected $registry;

	private $nameSpace;

	public function __construct( $registry ) {
		$this->registry = $registry;

		$this->nameSpace = $this->registry->get( 'config' )->nameSpace;

		add_action( 'widgets_init', [ $this, 'initWidgets' ] );
	}

	public function loadMaintenance() {
		$className = $this->nameSpace . '\\Core\\Maintenance';

		register_activation_hook( $this->registry->get( 'config' )->path . '/wordpress-review-plugin.php', [
			$className,
			'activate'
		] );
		register_deactivation_hook( $this->registry->get( 'config' )->path . '/wordpress-review-plugin.php', [
			$className,
			'deactivate'
		] );
		register_uninstall_hook( $this->registry->get( 'config' )->path . '/wordpress-review-plugin.php', [
			$className,
			'uninstall'
		] );
	}

	/**
	 * Hook widgets_init
	 */
	public function initWidgets() {
		$path = $this->registry->get( 'config' )->path . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'Widget';

		if ( ! is_dir( $path ) ) {
			return;
		}

		$widgets = scandir( $path, SCANDIR_SORT_NONE );

		foreach ( $widgets as $widget ) {
			if ( ! \in_array( $widget, [
					'.',
					'..'
				] ) && is_file( $path . DIRECTORY_SEPARATOR . $widget ) ) {
				$widget = basename( $widget, '.php' );
				$class  = $this->registry->get( 'config' )->nameSpace . '\\Widget\\' . $widget;
				register_widget( $class );
			}
		}
	}

	public function config( $name ) {
		$fullName = $this->nameSpace . '\\config\\' . $name;
		$config   = new $fullName();

		$sanitizeName = str_replace( '\\', '', $name );

		$this->registry->set( $sanitizeName . 'Config', $config );
	}

	public function controller( $name ) {
		$fullName   = $this->registry->get( 'config' )->nameSpace . '\\Controller\\' . $name;
		$controller = new $fullName( $this->registry );

		$sanitizeName = str_replace( '\\', '', $name );

		$this->registry->set( $sanitizeName . 'Controller', $controller );
	}

	public function model( $name ) {
		$fullName = $this->nameSpace . '\\Model\\' . $name;
		$model    = new $fullName( $this->registry );

		$sanitizeName = str_replace( '\\', '', $name );

		$this->registry->set( $sanitizeName . 'Model', $model );
	}

	public function view( $name, $data, $callback = null ) {
		$template = new Template( $this->registry, $callback );

		foreach ( $data as $key => $value ) {
			$template->set( $key, $value );
		}


		return $template->render( $name );
	}
}