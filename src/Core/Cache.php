<?php
/**
 * Wapplication Framework.
 * Framework for Wordpress.
 *
 * @category   Application
 * @package    Core
 * @author     Vladimir Zubko <vozubko@gmail.com>
 * @copyright  2018 Vladimir Zubko
 * @license    https://www.gnu.org/licenses/gpl-2.0.txt
 * @version    1.0
 * @link       https://vauko.com
 * @since      File available since Release 1.0
 * @deprecated
 */

namespace StudentMarketing\Core;

/**
 * Class Cache Caching data in the filesystem.
 * @package StudentMarketing\Common
 */
class Cache {
	/**
	 * The root cache directory.
	 * @var string
	 */
	private $path = '/tmp/cache';
	private $lifeTime = 3600;

	/**
	 * Creates a Cache object
	 *
	 * @param array $options
	 */
	public function __construct( array $options = [] ) {
		$available_options = [ 'path' ];
		foreach ( $available_options as $name ) {
			if ( isset( $options[ $name ] ) ) {
				$this->$name = $options[ $name ];
			}
		}
	}

	/**
	 * Fetches an entry from the cache.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function get( $id ) {
		$result = $this->loadFileById( $id );

		return $result ? $result->data : false;
	}

	public function getLifeTime( $id ) {
		$result = $this->loadFileById( $id );

		return $result ? $result->lifeTime : false;
	}

	/**
	 * Puts data into the cache.
	 *
	 * @param string $id
	 * @param mixed $data
	 * @param int $time
	 *
	 * @return bool
	 */
	public function save( $id, $data, $time = null ) {
		$dir = $this->getDirectory( $id );
		if ( ! is_dir( $dir ) ) {
			if ( ! mkdir( $dir, 0755, true ) ) {
				return false;
			}
		}
		$fileName   = $this->getFileName( $id );
		$lifeTime   = time() + ( is_null( $time ) ? $this->lifeTime : $time );
		$serialized = serialize( $data );
		$result     = file_put_contents( $fileName, $lifeTime . PHP_EOL . $serialized );
		if ( $result === false ) {
			return false;
		}

		return true;
	}

	/**
	 * Deletes a cache entry.
	 *
	 * @param string $id
	 *
	 * @return bool
	 */
	public function delete( $id ) {
		$fileName = $this->getFileName( $id );

		if ( file_exists( $fileName ) ) {
			return unlink( $fileName );
		}

		return true;
	}

	/**
	 * Delete all cache.
	 *
	 * @return bool
	 */
	public function deleteAll() {
		if ( is_dir( $this->path ) ) {
			return $this->wipeCache( $this->path );
		}

		return true;
	}

	// ------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------

	protected function loadFileById( $id ) {
		$fileName = $this->getFileName( $id );

		if ( ! is_file( $fileName ) || ! is_readable( $fileName ) ) {
			return false;
		}

		$lines    = file( $fileName );
		$lifeTime = array_shift( $lines );
		$lifeTime = (int) trim( $lifeTime );

		if ( $lifeTime !== 0 && $lifeTime < time() ) {
			@unlink( $fileName );

			return false;
		}

		$serialized = join( '', $lines );
		$data       = unserialize( $serialized );

		$result           = new \stdClass();
		$result->lifeTime = $lifeTime;
		$result->data     = $data;

		return $result;
	}

	/**
	 * Fetches a directory to store the cache data
	 *
	 * @param string $id
	 *
	 * @return string
	 */
	protected function getDirectory( $id ) {
		$hash = sha1( $id, false );
		$dirs = [
			$this->getCacheDirectory(),
			substr( $hash, 0, 2 ),
			substr( $hash, 2, 2 )
		];

		return join( DIRECTORY_SEPARATOR, $dirs );
	}

	/**
	 * Fetches a base directory to store the cache data
	 *
	 * @return string
	 */
	protected function getCacheDirectory() {
		return $this->path;
	}

	/**
	 * Fetches a file path of the cache data
	 *
	 * @param string $id
	 *
	 * @return string
	 */
	protected function getFileName( $id ) {
		$directory = $this->getDirectory( $id );
		$hash      = sha1( $id, false );
		$file      = $directory . DIRECTORY_SEPARATOR . $hash . '.cache';

		return $file;
	}

	/**
	 * Wipe cache.
	 *
	 * @param string $path
	 *
	 * @return bool
	 */
	private function wipeCache( $path ) {
		$files = array_diff( scandir( $path ), array( '.', '..' ) );
		foreach ( $files as $file ) {
			( is_dir( "$path/$file" ) ) ? $this->wipeCache( "$path/$file" ) : unlink( "$path/$file" );
		}

		return rmdir( $path );
	}
}